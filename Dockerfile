FROM sutty/sdk:latest
MAINTAINER "f <f@sutty.nl>"
ENV VERSION 2.7.2

RUN apk add --no-cache git nodejs nodejs-npm yarn tzdata libxslt-dev libxml2-dev
RUN apk add --no-cache ruby-dev ruby-bundler ruby-json ruby-bigdecimal ruby-rake ruby-etc
RUN apk add --no-cache postgresql-dev sqlite-dev mariadb-dev
RUN apk add --no-cache linux-headers
RUN test "$VERSION" = `ruby -e 'puts RUBY_VERSION'`

RUN addgroup -g 82 -S www-data
RUN adduser -s /bin/sh -G www-data -h /home/app -D app
RUN install -dm 2750 -o app -g www-data /home/app/sutty

# https://github.com/rubygems/rubygems/issues/2918
# https://gitlab.alpinelinux.org/alpine/aports/issues/10808
COPY ./rubygems-platform-musl.patch /tmp/
RUN patch -d /usr/lib/ruby/2.7.0 -Np 0 -i /tmp/rubygems-platform-musl.patch

ENTRYPOINT /bin/sh
